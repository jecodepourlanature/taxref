Taxref
======

Cette bibliothèque permet d'accéder à l'API TAXREF-web du Muséum National d'Histoire Naturelle et de naviguer dans la classification.

`Taxref-web <https://taxref.mnhn.fr/taxref-web/accueil>`_

Quick start
-----------

Installation:

.. code-block:: console

    $ pip install https://framagit.org/jecodepourlanature/taxref/-/archive/master/taxref-master.zip

ou

.. code-block:: console

    $ pipenv install https://framagit.org/jecodepourlanature/taxref/-/archive/master/taxref-master.zip

Exemple:

.. code-block:: python
    
    import taxref
    
    sp=taxref.Taxon(79303) #Pipistrellus kuhlii
    print(sp.scientificName)
    
    genus=sp.up() 
    print(genus) #Pipistrellus
    
    order=sp.up().up().up() 
    print(order) #Chiroptera
    
    q=taxref.Query('Pipistrellus')
    print(q.all()[0])
    for e in q.first().children:
        print(e)
    