from setuptools import setup,find_packages

setup(name='taxref',
      version='0.1',
      description="Cette bibliothèque permet d'accéder à l'API TAXREF-web du Muséum National d'Histoire Naturelle et de naviguer dans la classification.",
      url='https://framagit.org/jecodepourlanature/taxref',
      author='Jean-Baptiste Desbas',
      author_email='jb.desbas@gmail.com',
      license='MIT',
      packages=find_packages(),
      install_requires=["requests"]
      )
