"""
https://taxref.mnhn.fr/taxref-web/api/doc

Permet d'interoger l'API TAXREF du MNHN

"""

__version__ = "0.1"

from taxref.taxon import Taxon
from taxref.query import Query

__all__=['Taxon','Query']
