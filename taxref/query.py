from taxref.client import Client
from taxref.taxon import Taxon

client=Client()

class Query(object):
    """Rechercher des taxons dans taxref"""
    def __init__(self,searchString): #TODO ajouter les critères optionnel 'taxonomicRanks' et 'territories'
        self.searchString=str(searchString)
        self._cache=dict(data=list(),nb=0)
        #TODO lister les fullNameHtml trouvés et placer les donnees connues dans les Taxons
        #TODO ajouter un iterateur
    
    def __repr__(self):
        return '<Query \'{}\'>'.format(self.searchString)

    def all(self,nb=200):
        if self._cache['nb'] < nb and len(self._cache['data']) == self._cache['nb']:
            response=client.get_json('taxa/autocomplete',{'term':self.searchString,'size':nb})\
                        .get("_embedded",dict())\
                        .get('taxa',list())
            self._cache['data']=[Taxon(r['id']) for r in response]
            self._cache['nb']=nb
            assert len(self._cache['data']) <= self._cache['nb']
        return self._cache['data'][0:nb]
    
    def first(self):
        result=self.all(1)
        if len(result) > 0 :
            return result[0]
        else :
            return None

