import requests
from os.path import join

BASE_URL='https://taxref.mnhn.fr/api/'

#TODO Gérer les erreurs, timeout, etc.

class Client(object):
    def __init__(self,base_url=BASE_URL):
        self.base_url=base_url
    
    def get_json(self,rel_url,params=dict()):
        url=join(self.base_url,rel_url)
        r=requests.get(url,params=params)
        #print(r.url)
        return r.json()
