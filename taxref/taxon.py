from os.path import join

from taxref.client import Client

client=Client()

class Taxon(object):
    """
        Cette classe représente un taxon.
    """
    def __init__(self,id,data=dict()):
        self.id = None
        self.fuzzyMatch = None
        if type(id) is int:
            self.id = id
        else : #chaîne de caractère en général
            self.fuzzyMatch = id
        self._properties = None
        self._children = None
        self._parents = None
        if len(data) > 0:
            self._properties = data
            assert self.id == data['id']
            #verifier l'id passé en data ?
    
    def __getattr__(self, attr): #a garder ?
        return self.properties[attr]
    
    def __dir__(self):
        return self.properties.keys()
    
    def __repr__(self):
        return '<Taxon {}>'.format(self.id)
    
    def __str__(self):
        return self.properties['scientificName']
    
    def _clear(self):
        """Supprimer les attributs du taxon (necessitera d'intéroger l'API à nouveau)"""
        self._properties = None
        self._children = None
        self._parents = None
        #Il existe une façon de recreer un objet neuf ?
    
    @property
    def properties(self):
        if self._properties is None :
            if self.id is not None : #Accès direct par ID
                self._properties = client.get_json(join('taxa',str(self.id)))
            else : #Recherche par orthographe approché #TODO ajouté la possibilité de recherche par "autocomplete"
                self._properties = client.get_json(join('taxa','fuzzyMatch'),params={'term':str(self.fuzzyMatch)})\
                                .get("_embedded",dict())\
                                .get('taxa',list())[0] #TODO erreur si pas d'occurence trouvé ?
                self.id=self._properties['id']
        return self._properties
    
    def isRef(self):
        """ Indique si c'est un taxon de référence """
        return self.properties.get('referenceId') == self.properties.get('id')

    def up(self):
        """ Retourne le taxon parent """
        return Taxon(self.properties.get('parentId'))
    
    def children(self):
        """ Liste des taxon enfants """
        if self.id is None :
            self.properties.get('id') #déclenche la requete
        if self._children is None:
            result = client.get_json(join('taxa',str(self.id),'children'))\
                                .get("_embedded",dict())\
                                .get('taxa',list())
            self._children = [Taxon(t['id'],data=t) for t in result]
        return self._children
    
    def parents(self):
        """ Liste des taxon parents """
        if self.id is None :
            self.properties.get('id') #déclenche la requete
        if self._parents is None:
            result = client.get_json(join('taxa',str(self.id),'classification'))\
                                .get("_embedded",dict())\
                                .get('taxa',list())
            self._parents = [Taxon(t['id'],data=t) for t in result]
        return self._parents
